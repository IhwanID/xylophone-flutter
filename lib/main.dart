import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {

  void playNote(int soundNumber){
    final player = AudioCache();
    player.play('note$soundNumber.wav');
  }

  Expanded buildExpanded({Color color, int num}){
    return  Expanded(
      child: FlatButton(
        color: color,
        onPressed: () {
          playNote(num);
        },
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buildExpanded(color: Colors.red, num: 1),
              buildExpanded(color: Colors.blue, num: 2),
              buildExpanded(color: Colors.teal, num: 3),
              buildExpanded(color: Colors.yellow, num: 4),
              buildExpanded(color: Colors.limeAccent, num: 5),
              buildExpanded(color: Colors.purple, num: 6),
              buildExpanded(color: Colors.lightBlue, num: 7)
            ],
          ),
        ),
      ),
    );
  }
}
